const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/playground",{useNewUrlParser:true})
.then(() => console.log("Connected to mongodb"))
.catch(err => console.error("Could not connected to Mongodb",err))

const courseSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        minlength:5,
        maxlength:255
    },
    category:{
        type:String,
        enum:['web', 'mobile', 'network']
    },
    author:String,
    tags:[String],
    date:{type:Date, default:Date.now},
    isPublished:Boolean,
    price:{
        type:Number,
        required:function(){return this.isPulished;},
        min:10,
        max:200
    },

});

const Course = mongoose.model("Course",courseSchema);

async function createCourse(){
    const course = new Course({
        name:"Node js Course",
        author:"Mosh",
        category:'_',
        tags:['test','backend'],
        isPublished:true,
        price:15
    
    })
    try{
        // await course.validate();
        const result = await course.save();
        console.log(result)
    }
    catch(ex){
        console.log(ex.message);
    }
    
}
createCourse()

async function getCourse(){
    const pageNumber = 2;
    const pageSize = 10;
    const courses = await Course.find({tags:{$in:['frontend','backend']},isPublished:true})
    .limit(10)
    .sort({name:1})
    .select('name author isPublished')
    
    console.log(courses);
}

// getCourse();

// async function updateCourse(id){
//     const course = await Course.findById(id);
//     if(!course) return;
//     course.isPublished = true;
//     course.author = "Auther auther";

//     const result = await course.save();
//     console.log(result);
// }

// updateCourse('5d723836e2a3600362a2f5f6');

async function updateCourse(id){
    const result = await Course.findByIdAndUpdate(id, {
        $set:{
            auther:'Jack',
            isPublished:true}
        },{new:true});
    
    console.log(result);
}

// updateCourse('5d72381a7964230361488494');

async function removeCourse(id){
    // const result = await Course.deleteOne({_id:id});
    const course = await Course.findByIdAndRemove(id)
    console.log(course);
    // console.log(result);
}

// removeCourse('5d72381a7964230361488494');